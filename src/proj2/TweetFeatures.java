package proj2;

import java.util.SortedMap;
import java.util.TreeMap;

public class TweetFeatures {

	private Tweet tweet = null;
	private SortedMap<String, Integer> toponyms = new TreeMap<>();

	public TweetFeatures(Tweet tweet) {
		super();
		this.tweet = tweet;
	}

	public TweetFeatures(Tweet tweet, SortedMap<String, Integer> toponyms) {
		super();
		this.tweet = tweet;
		this.toponyms = toponyms;
	}

	public Tweet getTweet() {
		return tweet;
	}

	public void setTweet(Tweet tweet) {
		this.tweet = tweet;
	}

	public SortedMap<String, Integer> getToponyms() {
		return toponyms;
	}

	public void setToponyms(SortedMap<String, Integer> toponyms) {
		this.toponyms = toponyms;
	}
	
	public String toString() {
		return tweet + ", " + toponyms;
	}
	
}
