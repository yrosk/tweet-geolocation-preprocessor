package proj2;

public class Tweet {
	
	public enum City {
		B, H, SD, Se, W, UNKNOWN;
	}

	private long userid;
	private long tweetid;
	private String content;
	private City city;

	public Tweet(long userid, long tweetid, String content, City city) {
		super();
		this.userid = userid;
		this.tweetid = tweetid;
		this.content = content;
		this.city = city;
	}
	
	public long getUserid() {
		return userid;
	}
	public void setUserid(long userid) {
		this.userid = userid;
	}
	public long getTweetid() {
		return tweetid;
	}
	public void setTweetid(long tweetid) {
		this.tweetid = tweetid;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public City getCity() {
		return city;
	}
	public void setCity(City city) {
		this.city = city;
	}
	
	public String toString() {
		return userid + ", " + tweetid + ", " + content + ", " + city;
	}
}