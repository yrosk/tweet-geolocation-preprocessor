package proj2;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import com.bericotech.clavin.ClavinException;
import com.bericotech.clavin.GeoParser;
import com.bericotech.clavin.GeoParserFactory;
import com.bericotech.clavin.gazetteer.GeoName;
import com.bericotech.clavin.resolver.ResolvedLocation;

import proj2.Tweet.City;

/**
 * COMP90049 Knowledge Technologies
 * Semester 2, 2016
 * Project 2: Geolocation of Tweets with Machine Learning
 * 
 * @author Skye Rajmon McLeman
 *
 */
public class TweetFeatureEngineerer {

	public static void main(String[] args) {
		TweetFeatureEngineerer tng = new TweetFeatureEngineerer();
		tng.run();
	}

	public void run() {
		String trainTweetFile = "data/tweets/train-tweets.txt";
		String devTweetFile = "data/tweets/dev-tweets.txt";
		String testTweetFile = "data/tweets/test-tweets.txt";

		String trainARFFOutputFile = "output/train.arff";
		String devARFFOutputFile = "output/dev.arff";
		String testARFFOutputFile = "output/test.arff";

//		String trainTweetFile = "data/tweets/subset-train-tweets.txt";
//		String devTweetFile = "data/tweets/subset-dev-tweets.txt";
//		String testTweetFile = "data/tweets/subset-test-tweets.txt";
//
//		String trainARFFOutputFile = "output/subset-train.arff";
//		String devARFFOutputFile = "output/subset-dev.arff";
//		String testARFFOutputFile = "output/subset-test.arff";

		//		 Stores index of each unique toponym for creating ARFF file
		SortedMap<String, Integer> toponymIndices = new TreeMap<>();

		// First process training data
		System.out.println("Processing training data and building toponym list");
		SortedMap<Long, TweetFeatures> trainTweets = loadTweets(trainTweetFile);
		processTrainingData(trainTweets, toponymIndices);  // actually intializes the toponym map as well
		writeARFFOutputFile(trainARFFOutputFile, trainTweets, toponymIndices);

		// Now process dev data
		System.out.println("Processing dev data");
		SortedMap<Long, TweetFeatures> devTweets = loadTweets(devTweetFile);
		processDevOrTestData(devTweets, toponymIndices);  // uses the toponym map from findAnd... to only use those features
		writeARFFOutputFile(devARFFOutputFile, devTweets, toponymIndices);
		
		// And test data
		System.out.println("Processing test data");
		SortedMap<Long, TweetFeatures> testTweets = loadTweets(testTweetFile);
		processDevOrTestData(testTweets, toponymIndices);  // uses the toponym map from findAnd... to only use those features
		writeARFFOutputFile(testARFFOutputFile, testTweets, toponymIndices);
	}


	// Returns sorted map of tweet files loaded into memory.
	public SortedMap<Long, TweetFeatures> loadTweets(String tweetFilePath) {
		SortedMap<Long, TweetFeatures> tweets = new TreeMap<>();

		try (FileReader fr = new FileReader(tweetFilePath);
				BufferedReader br = new BufferedReader(fr)) {
			String line = null;
			try {
				while ((line = br.readLine()) != null) {
					String[] split = line.split("\t");
					long userid = Long.parseLong(split[0]);
					long tweetid = Long.parseLong(split[1]);
					String content = split[2];
					City city = null;
					if (split[3].equals("?")) {
						city = City.UNKNOWN;
					} else {
						city = City.valueOf(split[3]);
					}
					Tweet newTweet = new Tweet(userid, tweetid, content, city);
					tweets.put(tweetid, new TweetFeatures(newTweet));
				}
			} catch (IOException ex) {
				System.err.println("Error reading file: " + tweetFilePath);
				System.exit(0);
			}
		} catch (FileNotFoundException ex) {
			System.err.println("Unable to open file: " + tweetFilePath);
			System.exit(0);
		} catch (IOException ioex) {
			System.err.println("Error closing file: " + tweetFilePath);
			System.exit(0);
		}

		System.out.println(tweets.size() + " tweets successfully loaded");

		return tweets;
	}

	public void printToponymMap(SortedMap<String, Integer> toponymMap) {
		System.out.println("toponymMap: ");
		for (String toponym : toponymMap.keySet()) {
			System.out.println(toponym + ", " + toponymMap.get(toponym));
		}
	}

	public void printTweets(SortedMap<Long, TweetFeatures> tweets) {
		System.out.println("Tweets: ");
		for (TweetFeatures tweet : tweets.values()) {
			System.out.println(tweet);
		}
	}

	// Scans tweets and outputs all unique toponyms found to specified file.
	public void processTrainingData(SortedMap<Long, TweetFeatures> tweets, SortedMap<String, Integer> toponymIndices) {
		String indexDirectoryPath = "/input/directory/path/here/filename";
		// Instantiate CLAVIN GeoParser
		GeoParser parser = null;
		try {
			parser = GeoParserFactory.getDefault(indexDirectoryPath, false);
		} catch (ClavinException ce) {
			ce.printStackTrace();
			System.exit(0);
		}

		int numTweetProcessing = 0;  // keeps track of number of tweets processed
		int featureIndex = 1;  // tweet_id will have index 1, so start at 2 for toponym attributes
		for (TweetFeatures tweet : tweets.values()) {
			numTweetProcessing++;
			System.out.println("Processing training tweet " + numTweetProcessing + " of " + tweets.size() + "...");
			String tweetContent = tweet.getTweet().getContent();
			List<ResolvedLocation> resolvedLocations = null;
			SortedMap<String, Integer> tweetMap = new TreeMap<>();
			try {
				resolvedLocations = parser.parse(tweetContent);
				if (!resolvedLocations.isEmpty()) {
					for (ResolvedLocation resolvedLocation : resolvedLocations) {
						GeoName geoName = resolvedLocation.getGeoname();
						// Include GeonameID to produce unique value, because different locations can have same name.
						String toponymID = geoName.getGeonameID() + "-" + geoName.getAsciiName();
						toponymID = toponymID.replace("\"", "");  // some geonames have quotation marks,
						// which causes issues with ARFF format
						//						toponymID = toponymID.replace(" ", "_");
						// Add to map of all toponyms
						if (!toponymIndices.containsKey(toponymID)) {
							toponymIndices.put(toponymID, featureIndex);
							featureIndex++;  // only increase index if unique toponymID found
						}
						int count = tweetMap.containsKey(toponymID) ? tweetMap.get(toponymID) : 0;
						tweetMap.put(toponymID, count + 1);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(0);
			}
			tweet.setToponyms(tweetMap);
		}
		System.out.println("Finished adding toponym maps to tweets in memory.");
	}

	// Use when already have built up toponymMap
	public void processDevOrTestData(SortedMap<Long, TweetFeatures> tweets, SortedMap<String, Integer> toponymIndices) {
		// Instantiate CLAVIN GeoParser
		GeoParser parser = null;
		try {
			parser = GeoParserFactory.getDefault("/home/skypha/workspace/CLAVIN/IndexDirectory", false);
		} catch (ClavinException ce) {
			ce.printStackTrace();
			System.exit(0);
		}

		int numTweetProcessing = 0;  // keeps track of number of tweets processed
		int featureIndex = 1;  // tweet_id will have index 1, so start at 2 for toponym attributes
		for (TweetFeatures tweet : tweets.values()) {
			numTweetProcessing++;
			System.out.println("Processing dev/test tweet " + numTweetProcessing + " of " + tweets.size() + "...");
			String tweetContent = tweet.getTweet().getContent();
			List<ResolvedLocation> resolvedLocations = null;
			SortedMap<String, Integer> tweetMap = new TreeMap<>();
			try {
				resolvedLocations = parser.parse(tweetContent);
				if (!resolvedLocations.isEmpty()) {
					for (ResolvedLocation resolvedLocation : resolvedLocations) {
						GeoName geoName = resolvedLocation.getGeoname();
						// Include GeonameID to produce unique value, because different locations can have same name.
						String toponymID = geoName.getGeonameID() + "-" + geoName.getAsciiName();
						toponymID = toponymID.replace("\"", "");  // some geonames have quotation marks,
						// which causes issues with ARFF format
						//						toponymID = toponymID.replace(" ", "_");
						// Only add features that were discovered in training data
						if (toponymIndices.containsKey(toponymID)) {
							int count = tweetMap.containsKey(toponymID) ? tweetMap.get(toponymID) : 0;
							tweetMap.put(toponymID, count + 1);
						} // else ignores any other toponyms
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(0);
			}
			tweet.setToponyms(tweetMap);
		}
		System.out.println("Finished processing dev/test data.");	
	}

	// Open output file
	public Writer openOutputFile(String outputFile) {
		Writer writer = null;
		try {
			writer = new BufferedWriter(new OutputStreamWriter( new FileOutputStream(outputFile), "utf-8"));
		} catch (IOException ioex) {
			System.err.println("Error opening file for output: " + outputFile);
			ioex.printStackTrace();
			System.exit(0);
		}	

		return writer;
	}	

	// Close the file
	public void closeOutputFile(Writer writer) {
		try {
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(0);
		}
	}

	public void writeARFFOutputFile(String outputFile, SortedMap<Long, TweetFeatures> tweets, SortedMap<String, Integer> toponymIndices) {
		Writer writer = openOutputFile(outputFile);
		writeARFFHeader(writer, toponymIndices);
		writeARFFInstances(writer, tweets, toponymIndices);
		closeOutputFile(writer);
	}

	public void writeARFFHeader(Writer writer, SortedMap<String, Integer> toponymIndices) {
		try {
			writer.write("@RELATION features-output\n");
			writer.write("@ATTRIBUTE tweetid NUMERIC\n");
			// Reverse the map so we can print each attribute in order of index
			SortedMap<Integer, String> reversedMap = new TreeMap<>();
			for (String toponym : toponymIndices.keySet()) {
				reversedMap.put(toponymIndices.get(toponym), toponym);
			}
			for (String toponym : reversedMap.values()) {
				writer.write("@ATTRIBUTE \"" + toponym + "\" NUMERIC\n");
			}
			writer.write("@ATTRIBUTE location {B,H,SD,Se,W}\n");
			writer.write("@DATA\n");
		} catch (IOException e) {
			System.err.println("Error writing to ARFF output file");
			System.exit(0);
		}
	}

	public void writeARFFInstances(Writer writer, SortedMap<Long, TweetFeatures> tweets, SortedMap<String, Integer> toponymIndices) {
		for (TweetFeatures tweet : tweets.values()) {
			writeARFFInstance(writer, tweet, toponymIndices);
		}
	}

	public void writeARFFInstance(Writer writer, TweetFeatures tweet, SortedMap<String, Integer> toponymIndices) {
		int locationIndex = toponymIndices.size() + 1;
		try {
			writer.write("{0 " + Long.toString(tweet.getTweet().getTweetid()));
			SortedMap<Integer, Integer> indexToFreq = new TreeMap<>();
			for (Map.Entry<String, Integer> entry : tweet.getToponyms().entrySet()) {
				int featureIndex = toponymIndices.get(entry.getKey());
				indexToFreq.put(featureIndex, entry.getValue());
				// Put into another map, this time with index as key so
				// we can iterate through it sorted by index which is
				// necessary for ARFF files
			}
			for (Map.Entry<Integer, Integer> entry : indexToFreq.entrySet()) {
				writer.write(", " + entry.getKey() + " " + entry.getValue());
			}
			writer.write(", " + locationIndex + " ");
			if (tweet.getTweet().getCity() == City.UNKNOWN) {
				writer.write("?}\n");
			}
			else {
				writer.write(tweet.getTweet().getCity() + "}\n");
			}
		} catch (IOException ioex) {
			System.err.println("Error writing to ARFF ouput file");
			System.exit(0);
		}
	}
}