#!/usr/bin/perl
if ($#ARGV==-1) {die "Usage: perl weka-to-kaggle.pl weka-output-file\n"}

# Let's look for the tweet ids
$path="";
# Is there a test-tweets in this directory?
if (-e "./test-tweets.txt") {
	$path="./";
}
# Are we on the server?
elsif (-e "/home/subjects/comp90049/2016-sm2/project2/tweets/test-tweets.txt") {
	$path="/home/subjects/comp90049/2016-sm2/project2/tweets/";
}
# Let's load the ids, in case we need them - this might waste a little memory
@testids=();
if ($path) {
	if (not open (my $ID, $path."test-tweets.txt")) {$path="";}
	else {
		while ($_=<$ID>) {
			/^[0-9]+\s+([0-9]+)\s/ or last;
			push(@testids,$1);
		}
		close($ID);
	}
}


open(my $IN,"$ARGV[0]") or die "Bad input filename $ARGV[0]\n";
open(my $OUT,">$ARGV[0].kaggled") or die "Could not open output file $ARGV[0].kaggled\n";
print $OUT "Id,Category\n";

while ($_=<$IN>) {
	if (/^=== Predictions on test/) {last;}
}
$line="";
while ($line=<$IN>) {
	chomp($line);
	if (not $line=~/^.?$/) {last;}
}
$line=~/^\s*inst/ or die "Test header expected, $_ seen\n";
$ctr=-1;
while ($_=<$IN>) {
	$ctr++;
	if (/^.?$/) {last;}
	if (/^\s+[0-9]+\s+\?\s+[0-9]:([BHSDeW]{1,2})\s+[^(]*\(([0-9]+)\)/) {
		print $OUT "$2,$1\n";
	}
	# Weka 3.8 output format
	elsif (/^\s+[0-9]+\s+[0-9]:\?\s+[0-9]:([BHSDeW]{1,2})\s+[^(0-9]*\(?([0-9]+)\)?$/) {
		print $OUT "$2,$1\n";
    }
    # Weka 3.8 output format -- PlainText -p 1 (outputDistribution set to False)
	elsif (/^\s+[0-9]+\s+[0-9]:\?\s+[0-9]:([BHSDeW]{1,2})\s+[01](\.[0-9]+)?\s+\(([0-9]+)\)/) {
		print $OUT "$3,$1\n";
	}
	elsif (/^\s+[0-9]+\s+\?\s+[0-9]:([BHSDeW]{1,2})\s/ and $path){
		print $OUT "$testids[$ctr],$1\n";
	}
	# Weka 3.8 output format
	elsif (/^\s+[0-9]+\s+[0-9]:\?\s+[0-9]:([BHSDeW]{1,2})\s/ and $path){
		print $OUT "$testids[$ctr],$1\n";
	}
	elsif (/^\s+[0-9]+\s+\?\s+[0-9]:([BHSDeW]{1,2})\s/){
		die "No ID in the Weka output and no test-tweets available.\n";
	}
	else {die "Bad prediction line $_";}
}

close($OUT);
close($IN);
